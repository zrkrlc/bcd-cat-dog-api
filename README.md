# bcd-cat-dog-api
-- a RESTful API that combines the [Cat API](https://thecatapi.com/) and the [Dog API](https://dog.ceo/dog-api/)

## How to use
1. Run `npm install` in the root directory
2. Go to the `client` directory and also run `npm install` there
3. Now go back to the root directory and run `npm start`
4. Test the endpoints at port 3333: `/v1/breeds` and `/v1/images`. Note that when requesting dogs via ID you have to supply a `breed` query param because the Dog API does not let you access images by ID alone
5. Test the frontend at `http://localhost:3000`