import { useState, useEffect } from 'react';
import _ from 'lodash';
import './App.css';

function App() {
  const [dataBreeds, setDataBreeds] = useState(null);
  const [dataSingleBreed, setDataSingleBreed] = useState(null);
  const [dataImage, setDataImage] = useState(null);

  const [searchId, setSearchId] = useState("");
  const [searchSubmit, setSearchSubmit] = useState(false);
  
  const [query, setQuery] = useState({
    page: 0,
    limit: 10,
  });

  const [display, setDisplay] = useState("");
  
  useEffect(() => {
    const params = new URLSearchParams({
      api_key: process.env.REACT_APP_API_KEY,
      page: query.page,
      limit: query.limit
    });

    fetch("/v1/breeds?" + params)
    .then((res) => res.json())
    .then((data) => { 
      const pairsCat = _.zip(data.cats, _.fill(Array(data.cats.length), "😹"));
      const pairsDog = _.zip(data.dogs, _.fill(Array(data.dogs.length), "🐶"));

      const compareInt = (a, b) => b < a ? 1 : -1;
      const breeds = _.concat(pairsCat, pairsDog).sort(compareInt);

      return setDataBreeds(breeds.slice(0, Math.round(breeds.length / 2)));
    });


    // Fetch single breed
    if (searchSubmit && (searchId || searchId !== "")) {
      const breed = fetch("/v1/breeds/" + _.lowerCase(searchId) + '?' + params).then((r) => r.json());
      const image = fetch("/v1/images/" + _.lowerCase(searchId) + '?' + params).then((r) => r.json());

      if (breed) breed.then((data) => { 
        setDataSingleBreed(_.isEmpty(data.dogs) ? data.cats : data.dogs);
        setDisplay("singleBreed");
      });
      
      if (image) image.then((data) => {
        setDataImage(_.isEmpty(data.dogs) ? data.cats : data.dogs)
        setDisplay("image");
      });

      setSearchSubmit(false);
    }
  }, [query, searchSubmit]);


  return (
    <div className="App">
      <header className="App-header">
        <h1>BCD Cat-Dog API</h1>
        <div className="App-nav">

            <a onClick={() => {
                setDisplay(display !== "breeds" ? "breeds" : "")
                setSearchId("");
                }}>
              {display !== "breeds" ? "Show Breeds" : "Hide Breeds"}
            </a>

          <input 
            className="App-input"
            id="search-bar"
            placeholder="Or type a breed or ID here..."
            value={searchId}
            onChange={(e) => setSearchId(e.target.value)}
            onKeyPress={(e) => { if (e.key === 'Enter') {
              setSearchSubmit(true); 
              setQuery({page: 0, limit: query.limit});
            }}}>
          </input>

        </div>
      </header>
      <main className="App-main">
        {searchSubmit ? <h2>Searching...</h2> : null}
    
        {/* Load breeds data */}
        {dataBreeds && (display === "breeds") && !searchId ? 
          <div className="App-table">
           {dataBreeds.map((elem, index) => {
             return (
               <div className='App-table-item' key={index}>
                 <span>{index + 1}) {elem[0]}</span>
                 <span>{elem[1]}</span>
               </div>
             )})}
          </div> : null}

          {/* Load single breed data */}
          {dataSingleBreed && display === "singleBreed" ?
            <div className='App-grid'>
              {dataSingleBreed.map((elem, index) => {
                return (
                 <div className='App-grid-item' key={index}>
                   <img src={elem}></img>
                 </div>
              )}).slice(0, Math.min(query.limit, dataSingleBreed.length))}
            </div> : null}

          {/* Load image data */}
          {dataImage && display === "image" ?
            <div className='App-table'>
              {dataSingleBreed.map((elem, index) => {
                return (
                 <div className='App-table-item' key={index}>
                   <span>{index + 1}) {elem[0]}</span>
                   <span>{elem[1]}</span>
                 </div>
              )}).slice(0, Math.min(query.limit, dataImage.length))}
            </div> : null}

          

       </main>

      <footer className="App-footer">
        {display !== "" ?
          <div id="pagination">
            <input
              placeholder='Page'
              value={query.page}
              onChange={(e) => setQuery({limit: query.limit, page: e.target.value})}>
            </input>
            <input
              placeholder='Limit'
              value={query.limit}
              onChange={(e) => setQuery({page: query.page, limit: e.target.value})}>
            </input>
          </div> : null}
          <br />
        Made with ♥ using ReactJS and ExpressJS.
      </footer>
    </div>
  );
}

export default App;
