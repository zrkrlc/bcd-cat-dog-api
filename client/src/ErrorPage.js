import { useRouteError } from "react-router";
import { Link } from 'react-router-dom';

export default function ErrorPage() {
    const error = useRouteError();

    return (
        <>
            <header className="ErrorPage-header">
                <h1>Oh no! We encountered an error.</h1>
            </header>
            <main className="ErrorPage-main">
                <p><code>{error.statusText || error.message}</code></p>
                
                <Link to={'/'}>Back to App</Link>
            </main>
        </>
    );
}