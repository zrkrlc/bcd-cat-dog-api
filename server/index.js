import _ from 'lodash';
import fetch from 'node-fetch';
import express from 'express';
import dotenv from 'dotenv';

const config = dotenv.config();
const app = express();

// Serve "public" folder
app.use(express.static('public'))

const dataBreeds = async (page, limit) => { 
    let breedsCat = [];
    const dataCat = await 
        fetch('https://api.thecatapi.com/v1/breeds?' + new URLSearchParams({
            api_key: process.env.API_KEY,
            page: page,
            limit: limit
        }))
        .then((res) => res.json())
        .then((data) => { 
            data.forEach((elem) => breedsCat.push(elem.name))
        });

    let breedsDog = [];
    const dataDog = await
        fetch('https://dog.ceo/api/breeds/list/all')
        .then((res) => res.json())
        .then((data) => {
            let breeds = data.message;

            // Flattens breed array by combining sub-breeds
            for (const key in breeds) {
                if (breeds[key].length == 0) {
                    breedsDog.push(_.startCase(key));
                } else {
                    breeds[key].forEach((elem) => {
                        const fixedName = _.startCase(key + ' ' + elem);
                        breedsDog.push(fixedName);
                    })
                }
            }
        });
    
    // Paginates dog breeds
    const breedsDogPaginated = _.chunk(breedsDog, limit).slice(0, limit);

    return {'cats': breedsCat, 'dogs': breedsDogPaginated[page]};
};

const dataBreedSingle = async (id, page, limit) => { 
    let imagesCat = [];
    const dataCat = await 
        fetch('https://api.thecatapi.com/v1/images/search?' + new URLSearchParams({
            api_key: process.env.API_KEY,
            page: page,
            limit: limit,
            breed_ids: id
        }))
        .then((res) => res.json())
        .then((data) => {
            data.forEach((elem) => imagesCat.push(elem.url))
        })
        .catch((err) => err);

    let imagesDog = [];
    const dataDog = await
        fetch('https://dog.ceo/api/breed/' + id + "/images")
        .then((res) => res.json())
        .then((data) => {
            if (data.code != 404) imagesDog = data.message;
        })
        .catch((err) => err);
    
    // Paginates dog images
    const imagesDogPaginated = _.chunk(imagesDog, limit).slice(0, limit);

    if (imagesDog.length !== 0) {
        return {'cats': imagesCat, 'dogs': imagesDogPaginated[page]};
    } else {
        return {'cats': imagesCat, 'dogs': []};
    };
};

app.get('/v1/breeds', async (req, res) => {
    if (req.query.page && req.query.limit) {
            return res.send(await dataBreeds(req.query.page, req.query.limit))
    } else {
    return res.send(await dataBreeds(0, 10));
    }
});

app.get('/v1/breeds/:breed', async (req, res) => {
    if (req.query.page && req.query.limit) {
        return res.send(await dataBreedSingle(req.params.breed, req.query.page, req.query.limit));
    } else {
        return res.send(await dataBreedSingle(req.params.breed, 0, 10));
    }
});

const dataImageSingle = async (id, breed) => { 
    let imageCat = '';
    const dataCat = await 
        fetch('https://api.thecatapi.com/v1/images/' + id + '?' + new URLSearchParams({
            api_key: process.env.API_KEY,
        }))
        .then((res) => res.json())
        .then((data) => {imageCat = data.url})
        .catch((err) => err);

    let imageDog = '';
    const dataDog = await
        fetch('https://dog.ceo/api/breed/' + breed + "/images")
        .then((res) => res.json())
        .then((data) => {

            let images = data.message;
            for (let i = 0; i < images.length; i++) {
                if (images[i].includes(id)) imageDog = images[i];
            }
        })
        .catch((err) => console.log(err));
    
    return {'cats': imageCat, 'dogs': imageDog};
};

// Since the Dog API does not provide a clean way to query dog images by ID,
// you must supply a query param breed when requesting dogs
// Ex: /v1/images/n02088094_1003?breed=hound
app.get('/v1/images/:image', async (req, res) => {
    if (req.query.breed) {
        return res.send(await dataImageSingle(req.params.image, req.query.breed));
    } else {
        return res.send(await dataImageSingle(req.params.image, ""));
    }
});

app.get('*', (req, res) => {
    return res.send("Error: available endpoints at /v1/breeds, /v1/breeds/:breed, /v1/images/:image")
});

app.listen(process.env.PORT, () =>
  console.log(`Listening on port ${process.env.PORT}`),
);